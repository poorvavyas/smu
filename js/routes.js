/**
 * Defines the main routes in the application.
 * The routes you see here will be anchors '#/' unless specifically configured otherwise.
 */

define(['./app'], function (app) {
    'use strict';
    angular.module('app')
            .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('/home');
            $stateProvider
                .state('app', {
                    templateUrl: 'partials/app_template.html',
                    controller: 'appController'
                }).
                state('app.home',{
                    url:'/home',
                    templateUrl: 'partials/home.html',
                    controller: 'homeController' 
                })
        }]);
});
