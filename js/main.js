/**
 * configure RequireJS
 * prefer named modules to long paths, especially for version mgt
 * or 3rd party libraries
 */
require.config({

    paths: {
        'jquery':'../lib/jquery/dist/jquery',
        'angular': '../lib/angular/angular',
        'angular-route': '../lib/angular-route/angular-route',
        'domReady': '../lib/requirejs-domready/domReady',
        'ui-state':'../lib/angular-ui-router/release/angular-ui-router.min'
    },

    /**
     * for libs that either do not support AMD out of the box, or
     * require some fine tuning to dependency mgt'
     */
    shim: {
        'jquery':{
            exports:'jquery'
        },
        'angular': {
            exports: 'angular',
            deps:['jquery']
        },
        'angular-route': {
            deps: ['angular']
        },
        'ui-state':{
            deps:['angular']
        }
    },

    deps: [
        // kick start application... see bootstrap.js
        './bootstrap'
    ]
});
